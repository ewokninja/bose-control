#
# Small script to control Bose SoundTouch 20
# Updated: 20191226
# 

import sys
import libsoundtouch

##### CONFIG #####
ipaddr = '192.168.50.182'
high_vol_threshold = 50

### URLS ###
"""
To add new streams, add a name and a URL below. Their positions must match in each list.
"""
s_names = [
    'myfm',
    # 'station_name',
]
s_urls = [
    'http://ice23.securenetsystems.net/CJGM',
    # 'station_url',
]



### ---------------------------------------------------------------------------------
### Nothing configurable after this line
### ---------------------------------------------------------------------------------



##### INITIALIZATION #####
device = libsoundtouch.soundtouch_device(ipaddr)
presets = device.presets()

stms = zip(s_names, s_urls)
streams = dict(stms)
stream_indicies = [x for x,_ in stms]


# Usage function
def usage():
    print("Usage:\n\n" \
        "   bose [command/stream_name]\n\n" \
        "Valid Commands:\n\n" \
        "   on     power on the device\n" \
        "   off    power off the device\n" \
        "   aux    set the input to aux\n" \
        "   1-6    a preset number 1 through 6\n" \
        "   mute   toggle mute status\n" \
        "   volume set a volume level\n\n" \
        "Valid Stream Names:\n")
    for i in stream_indicies:
        print(i),
    

# If no arg was supplied we tell the user how to use this command.
if len(sys.argv) == 1:
    usage()
    exit()
else:
    arg = sys.argv[1]

def list_devices():
    devices = libsoundtouch.discover_devices(timeout=2)

    for i in devices:
        print(device.config.name + " - " + device.config.type)

        volume = device.volume()
        print(volume.actual)
        print(volume.muted)
        device.set_volume(30)

##### LOGIC #####
if isinstance(arg, str):        # Argument passed was a string
    if arg == "off":            # Argument passed was "off"
        print("Powering off Bose")
        device.power_off()
        exit()
    if arg == "on":             # Argument passed was "on"
        print("Powering on Bose")
        device.power_on()
        exit()
    if arg == "aux":            # Argument passed was "aux"
        print("Setting input to {}").format(arg)
        device.select_source_aux()
        exit()
    if arg in stream_indicies:  # Argument was a stream name in stream_indicies
        print("Playing station {}").format(arg)
        device.play_url(streams[arg])
        exit()

    if arg == "mute":           # Argument passed was mute\unmute
        print("Muting Bose")
        device.mute()
        exit()
    
    if arg == "volume":
        vol = input("Set volume to [0..100]: ")
        if 1 <= int(vol) <= 100:    # Check if input was between 0 - 100
            if int(vol) >= high_vol_threshold:      # Double check value entered was intended
                checkstr = "A volume of {} could be pretty loud, are you sure? [y/n] ".format(str(vol))
                yn = raw_input(checkstr).lower().strip()
                if str(yn) == "n":
                    print("Thought not... exiting.")
                    exit()

            print("Setting volume of Bose to {}").format(str(vol))
            device.set_volume(int(vol))
            exit()
        usage()                 # Input was invalid, show usage
        exit()

    
    ### Utility Arguments ###
    if arg == "list":
        list_devices()
        exit()
    
    arg = int(sys.argv[1])      # Maybe they entered a preset number
    if 1 <= arg <= 6:           # presets 1-6
        print("Setting input to preset {}").format(str(arg))
        device.select_preset(presets[arg-1])
        exit()
    
usage()                         # Argument entered wasn't valid
exit()
