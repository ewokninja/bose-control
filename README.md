# bose-control

Designed to be used locally on a network with a Bose SoundTouch unit.

## Configuration

Edit the section labeled CONFIG in bose.py to match your setup.

## Requirements

libsoundtouch and python 2.x

- pip install libsoundtouch
- pip install zeroconf==0.19.1

## Usage

From your chose command line, execute 'python bose.py'

